<?php

namespace Drupal\opigno_sms;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\sms\Direction;
use Drupal\sms\Entity\SmsMessage as SmsMessageEntity;
use Drupal\sms\Exception\PhoneNumberSettingsException;
use Drupal\sms\Message\SmsMessage;
use Drupal\sms\Provider\PhoneNumberProviderInterface;
use Drupal\sms\Provider\PhoneNumberVerification;
use Drupal\user\UserDataInterface;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;
use OTPHP\Factory;
use OTPHP\TOTP;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * OpignoSmsProvider class.
 */
class OpignoSmsProvider {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Phone number provider.
   *
   * @var \Drupal\sms\Provider\PhoneNumberProviderInterface
   */
  protected $phoneNumberProvider;

  protected $phoneNumberVerificationProvider;

  protected $opignoSmsEntityExtender;

  protected $userStorage;

  /**
   * Constructs a new OtpSmsUserLoginFormAlter.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data service.
   * @param \Drupal\sms\Provider\PhoneNumberProviderInterface $phone_number_provider
   *   The phone number provider.
   * @param \Drupal\opigno_sms\UserEntityExtender $opigno_sms_entity_extender
   *   The entity extender interface.
   * @param \Drupal\sms\Provider\PhoneNumberVerification $phone_number_verification_provider
   *   The phone number verification provider.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type_manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    RequestStack $request_stack,
    UserDataInterface $user_data,
    PhoneNumberProviderInterface $phone_number_provider,
    UserEntityExtender $opigno_sms_entity_extender,
    PhoneNumberVerification $phone_number_verification_provider,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->requestStack = $request_stack;
    $this->userData = $user_data;
    $this->phoneNumberProvider = $phone_number_provider;
    $this->opignoSmsEntityExtender = $opigno_sms_entity_extender;
    $this->phoneNumberVerificationProvider = $phone_number_verification_provider;
    $this->userStorage = $entity_type_manager->getStorage('user');
  }

  /**
   * Validate code by provisioning url.
   *
   * @param string $uri
   *   Provisioning url.
   * @param string $code
   *   Code string value.
   *
   * @return bool
   *   Is code is valid.
   */
  public function verifyCodeValue(string $uri, string $code): bool {
    if (!empty($uri) && !empty($code)) {
      try {
        $otp = Factory::loadFromProvisioningUri($uri);
        if (!$otp->verify($code)) {
          throw new \Exception('Invalid code');
        }
      }
      catch (\Exception $e) {
        // @codingStandardsIgnoreLine
        // \Drupal::messenger()->addMessage($otp->now());
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Provisioning URL string.
   */
  public function getProvisioningUri(): string {
    return (new TOTP('uri', NULL, $this->getOtpSmsLifetime()))->getProvisioningUri();
  }

  /**
   * Receive a pone  number setting from an sms framework.
   *
   * @param string $entity_type_id
   *   Entity type identifier.
   * @param string $bundle
   *   Bundle machine name.
   *
   * @return \Drupal\sms\Entity\PhoneNumberSettingsInterface|false
   *   Phone number settings.
   */
  public function getPhoneNumberSettings(string $entity_type_id, string $bundle) {
    try {
      if (!($phone_number_settings = $this->phoneNumberVerificationProvider->getPhoneNumberSettings($entity_type_id, $bundle))) {
        throw new PhoneNumberSettingsException('Config phone entity does not exist');
      }
    }
    catch (PhoneNumberSettingsException $e) {
      return FALSE;
    }
    return $phone_number_settings;
  }

  /**
   * {@inheritdoc}
   */
  public function maybeSendNewCode($phone_number, $code_activation) {
    if ($this->shouldSendSmsToUser($phone_number . $code_activation)) {
      $this->sendOtpSms($phone_number, $code_activation);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function sendOtpSms($phone_number, $code_activation) {
    $current_time = $this->getCurrentTime();

    $one_time_pass = $this->getOneTimePassword($code_activation);
    $code = $one_time_pass->at($current_time);

    $sms = (new SmsMessage())
      ->setMessage(sprintf('Your two factor code is %s', $code))
      ->setAutomated(FALSE);

    $sms_message = SmsMessageEntity::convertFromSmsMessage($sms)
      ->addRecipient($phone_number)
      ->setAutomated(FALSE)
      ->setDirection(Direction::OUTGOING);

    // @codingStandardsIgnoreLine
    \Drupal::service('sms.provider.default')->send($sms_message);

    $this->setLastOtpSms($phone_number);
  }

  /**
   * Get when a OTP SMS was last sent to a user.
   *
   * @param string $phone_number
   *   A phone number.
   *
   * @return null|int
   *   The current time, or NULL if the user has never been sent an OTP SMS.
   */
  protected function getLastOtpSms(string $phone_number) {
    return $this->userData
      ->get('opigno_sms', 0, 'last_sms' . crc32($phone_number));
  }

  /**
   * Set when a OTP SMS was last sent to a user.
   *
   * @param string $phone_number
   *   A phone number.
   * @param null|int $time
   *   The timestamp for when the OTP SMS was last sent, or NULL to use
   *   current time.
   */
  protected function setLastOtpSms(string $phone_number, $time = NULL) {
    $time = !isset($time) ? $this->getCurrentTime() : time();
    $this->userData
      ->set('opigno_sms', 0, 'last_sms' . crc32($phone_number), $time);
  }

  /**
   * How long to wait before sending a new SMS.
   *
   * @return int
   *   Number of seconds to wait before sending a new SMS.
   */
  protected function getOtpSmsLifetime() {
    return 3600;
  }

  /**
   * Determine whether a SMS can be sent to the phone number.
   *
   * @param string $phone_number
   *   A phone number.
   *
   * @return bool
   *   Whether a new OTP SMS should be sent to a user.
   */
  protected function shouldSendSmsToUser(string $phone_number) {
    $sms_lifetime = $this->getOtpSmsLifetime();
    $current_time = $this->getCurrentTime();
    $last_sms = $this->getLastOtpSms($phone_number);
    if (!$last_sms) {
      // User has never been sent a SMS.
      return TRUE;
    }
    else {
      return ($current_time - $sms_lifetime) > $last_sms;
    }
  }

  /**
   * Validates that user input is a valid phone number.
   *
   * @param string $phone_value
   *   User input of phone number.
   *
   * @return bool
   *   Checks that number valid.
   */
  public static function isValidMobileNumber(string $phone_value): bool {
    try {
      $phone_util = PhoneNumberUtil::getInstance();
      $phone_number = $phone_util->parse($phone_value);
      return $phone_util->isValidNumber($phone_number);
    }
    catch (NumberParseException $e) {
    }
    return FALSE;
  }

  /**
   * Get the current time.
   *
   * @return int
   *   The current time.
   */
  protected function getCurrentTime() {
    return $this->requestStack
      ->getCurrentRequest()
      ->server
      ->get('REQUEST_TIME');
  }

  /**
   * One time password provider getter.
   */
  private function getOneTimePassword($code_activation) {
    try {
      return Factory::loadFromProvisioningUri($code_activation);
    }
    catch (\Exception $e) {
      return $code_activation;
    }
  }

  /**
   * Entity extender getter.
   */
  public function getExtender($getUserCorrect) {
    return $this->opignoSmsEntityExtender->getInstance($getUserCorrect);
  }

  /**
   * {@inheritdoc}
   */
  public function loadEntityByPhoneNumber($to) {
    $users = $this->userStorage->loadByProperties(['mail' => $to]);
    if (!$users) {
      return FALSE;
    }
    $user = reset($users);
    return $this->getExtender($user);
  }

}
