<?php

namespace Drupal\opigno_sms\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\opigno_sms\OpignoSmsProvider;
use Drupal\telephone\Plugin\Field\FieldWidget\TelephoneDefaultWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twilio\Rest\Client;
use Twilio\Rest\Lookups;

/**
 * Plugin implementation of the 'opigno_sms_telephone_widget' widget.
 *
 * @FieldWidget(
 *   id = "opigno_sms_telephone_widget",
 *   module = "opigno_sms",
 *   label = @Translation("Opigno Sms Telephone Widget"),
 *   field_types = {
 *     "telephone"
 *   }
 * )
 */
class OpignoSmsTelephoneWidget extends TelephoneDefaultWidget implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Service "opigno_sms.otp_provider" definition.
   *
   * @var \Drupal\opigno_sms\OpignoSmsProvider
   */
  protected $opignoSmsOtpProvider;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    OpignoSmsProvider $opigno_sms_otp_provider,
    AccountProxy $current_user
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->opignoSmsOtpProvider = $opigno_sms_otp_provider;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('opigno_sms.otp_provider'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    if ($this->currentUser->hasPermission('by pass access to field mobile number')) {
      $element['value']['#disabled'] = FALSE;
      $element['value']['#description'] = '';
    }
    $element['#element_validate'][] = [$this, 'elementValidateNumber'];
    return $element;
  }

  /**
   * Element builder.
   */
  public function formElementWithCode(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $parents = [$items->getName(), $delta];
    $values = $form_state->getValues();
    $value = NestedArray::getValue($values, $parents);
    $element['original'] = [
      '#type' => 'value',
      '#value' => $items[$delta]->getValue() + [
        'value' => NULL,
        'verification_result' => NULL,
      ],
    ];
    $element['uri'] = [
      '#type' => 'value',
      '#default_value' => $value['uri'] ?: $this->opignoSmsOtpProvider->getProvisioningUri(),
    ];
    $element['code'] = [
      '#title' => 'Verification code',
      '#access' => (bool) $value['uri'],
      '#required' => TRUE,
      '#type' => 'textfield',
    ];
    if (!\Drupal::currentUser()
      ->hasPermission('by pass access to field mobile number')) {
      $element['#element_validate'][] = [$this, 'elementValidate'];
      $element['value']['#description'] = $this->t('Enter a phone number. A verification code will be sent as an SMS message.');
    }
    else {
      $element['value']['#description'] = '';
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function elementValidateNumber(array &$element, FormStateInterface $form_state): void {
    $parents = $element['#parents'];
    $values = &$form_state->getValues();
    $value = &NestedArray::getValue($values, $parents);
    $sms_gateway_storage = \Drupal::entityTypeManager()->getStorage('sms_gateway');
    /* @var \Drupal\sms\Entity\SmsGateway $entity_twilio */
    $entity_twilio = $sms_gateway_storage->load('twilio');
    try {
      $client = new Client($entity_twilio->get('settings')['account_sid'], $entity_twilio->get('settings')['auth_token']);
      $phone_number = $client->lookups->v1->phoneNumbers($value['value'])->fetch();
      $value['value'] = $phone_number->phoneNumber;
    }
    catch (\Exception $sms_gateway_storage) {
      $form_state->setError($element, 'Provided phone number is invalid.');
    }
  }

  /**
   * Validator of single element.
   *
   * @param array $element
   *   Element build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   */
  public function elementValidateWithCode(array &$element, FormStateInterface $form_state): void {
    $parents = $element['#parents'];
    $values = $form_state->getValues();
    $value = NestedArray::getValue($values, $parents);
    if (!empty($value['uri']) && !empty($value['code'])) {
      if (empty($value["code"])) {
        $form_state->setError($element, 'Code value cannot be an empty.');
      }
      if (empty($value["uri"])) {
        $form_state->setError($element, 'Code value cannot be verified.');
      }

      if (!$this->opignoSmsOtpProvider->verifyCodeValue($value["uri"], $value["code"])) {
        $form_state->setError($element, 'Entered code is incorrect or expired.');
      }
    }
    elseif (OpignoSmsProvider::isValidMobileNumber((string)$value["value"])) {
      $storage = &$form_state->getStorage();
      $storage['validate_code'] = TRUE;
      $form_state->setRebuild();
      $this->opignoSmsOtpProvider->maybeSendNewCode($value["value"], $value["uri"]);
    }

  }

}
