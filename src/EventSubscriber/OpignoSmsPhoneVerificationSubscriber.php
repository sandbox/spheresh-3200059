<?php

namespace Drupal\opigno_sms\EventSubscriber;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\sms\Entity\PhoneNumberVerification;
use Drupal\sms\Event\PhoneNumberVerificationEvent;
use Drupal\sms\Event\SmsEvents;
use Drupal\sms\Exception\PhoneNumberSettingsException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class OpignoSmsPhoneVerificationSubscriber.
 */
class OpignoSmsPhoneVerificationSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new OpignoSmsPhoneVerificationSubscriber object.
   */
  public function __construct(AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[SmsEvents::PHONE_VERIFICATION_PRE_PROCESS] = ['smsMessagePhoneVerificationPreProcess'];
    return $events;
  }

  /**
   * This method is called when the phone_verification process is dispatched.
   *
   * @param \Drupal\sms\Event\PhoneNumberVerificationEvent $event
   *   The dispatched event.
   */
  public function smsMessagePhoneVerificationPreProcess(PhoneNumberVerificationEvent $event): void {
    $phone_verification = $event->getPhoneVerification();
    $entity = $event->getEntity();
    try {
      $phone_number_settings = $event->getPhoneNumberSettings();
      $field_name = $phone_number_settings->getFieldName('phone_number');
      if (!empty($field_name)) {
        $items_original = isset($entity->original) ? $entity->original->{$field_name} : NULL;
      }
    }
    catch (PhoneNumberSettingsException $e) {
      // Missing phone number configuration for this entity.
    }
    if ($phone_verification instanceof PhoneNumberVerification) {
      $phone_verification->setCode(rand(100000, 999999));
      $allow_all_skip = TRUE;
      if (
        $allow_all_skip ||
        is_null($items_original) ||
        $this->currentUser->hasPermission('by pass access to field mobile number')
      ) {
        $phone_verification->setStatus(TRUE);
        $event->stopSendingVerification();
      }
    }
  }

}
