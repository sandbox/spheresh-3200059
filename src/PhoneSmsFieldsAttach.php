<?php

namespace Drupal\opigno_sms;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manage attaching the new field to the user entity.
 */
class PhoneSmsFieldsAttach implements ContainerInjectionInterface {

  use StringTranslationTrait;

  const FIELD_NAME = 'phone';

  const ENTITY_TYPE = 'user';

  const PROVIDER_NAME = 'opigno_sms';

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * Create an instance of UserFieldAttach.
   */
  public function __construct(EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager) {
    $this->entityDefinitionUpdateManager = $entityDefinitionUpdateManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.definition_update_manager')
    );
  }

  /**
   * Implements hook_entity_base_field_info_alter().
   */
  public function entityBaseFieldInfoAlter(&$fields, EntityTypeInterface $entity_type) {
    if ($entity_type->id() !== static::ENTITY_TYPE) {
      return;
    }
    $fields[static::FIELD_NAME] = $this->getBaseFieldDefinition();
  }

  /**
   * Get the base field definition for the one time password field.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   A base field definition for the one time password field.
   */
  protected function getBaseFieldDefinition() {
    $field = BaseFieldDefinition::create('telephone')
      ->setLabel($this->t('Phone number'))
      ->setName(static::FIELD_NAME)
      ->setProvider(static::PROVIDER_NAME)
      ->setTargetEntityTypeId(static::ENTITY_TYPE)
      ->setDescription($this->t('Phone number.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setInitialValue('')
      ->setDefaultValue('')
      ->addConstraint('UniqueField', [])
      ->setDisplayOptions('view', [
        'region' => 'content',
        'type' => 'telephone_link',
      ])
      ->setDisplayOptions('form', [
        'region' => 'content',
        'type' => 'opigno_sms_telephone_widget',
      ]);
    $this->applyDefaultFieldDefinition($field);
    return $field;
  }

  /**
   * Install the field definition.
   */
  public function installFieldDefinition($entity_id = NULL, $provider = NULL) {
    $this->entityDefinitionUpdateManager->installFieldStorageDefinition(static::FIELD_NAME, static::ENTITY_TYPE, static::PROVIDER_NAME, $this->getBaseFieldDefinition());
  }

  /**
   * Uninstall the field definition.
   */
  public function updateFieldStorageDefinition() {
    $this->entityDefinitionUpdateManager->updateFieldStorageDefinition($this->getBaseFieldDefinition());
  }

  /**
   * Uninstall the field definition.
   */
  public function uninstallFieldDefinition() {
    $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($this->getBaseFieldDefinition());
  }

  /**
   * Implements hook_entity_field_access().
   */
  public function entityFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    if ($field_definition->getName() === static::FIELD_NAME) {
      return AccessResult::allowed();
    }
    return AccessResult::neutral();
  }

  /**
   * Apply a provider to the field definition.
   */
  private function applyDefaultFieldDefinition(BaseFieldDefinition $field) {
    $field
      ->setProvider(static::PROVIDER_NAME);
    return $field;
  }

}
