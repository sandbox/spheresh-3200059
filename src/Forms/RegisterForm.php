<?php

namespace Drupal\opigno_sms\Forms;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\opigno_sms\MultiStepFormTrait;
use Drupal\opigno_sms\OpignoSmsProvider;
use Drupal\user\RegisterForm as DefaultRegisterForm;
use Drupal\user\UserData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * RegisterForm class.
 */
class RegisterForm extends DefaultRegisterForm {

  use MultiStepFormTrait;

  const PHONE_NUMBER_CODE_VERIFIED = 'phone_number_code_verified';

  /**
   * OtpSmsProvider service.
   *
   * @var \Drupal\opigno_sms\OpignoSmsProvider
   */
  protected $opignoSmsOtpProvider;

  /**
   * Phone field settings.
   *
   * @var string
   */
  protected $fieldName = '';

  /**
   * PhoneNumberSettings config.
   *
   * @var \Drupal\sms\Entity\PhoneNumberSettingsInterface|bool
   */
  protected $phoneNumberSettings;

  /**
   * ImmutableConfig config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $opignoSmsSmsconfig;

  /**
   * Request service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * RegisterForm constructor.
   */
  public function __construct(
    OpignoSmsProvider $opigno_sms_otp_provider,
    EntityRepositoryInterface $entity_repository,
    LanguageManagerInterface $language_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    TimeInterface $time = NULL,
    ConfigFactory $config_factory,
    UserData $user_data
  ) {
    parent::__construct($entity_repository, $language_manager, $entity_type_bundle_info, $time);
    $this->opignoSmsOtpProvider = $opigno_sms_otp_provider;
    $this->opignoSmsSmsconfig = $config_factory->get('opigno_sms.sms_config');
    $this->userData = $user_data;
  }

  /**
   * Container create.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('opigno_sms.otp_provider'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('config.factory'),
      $container->get('user.data')
    );
  }

  /**
   * Form builder.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    if (!$this->getSmsFieldSettings()) {
      return $form;
    }

    assert(!empty($this->fieldName), 'Phone number settings is incorrect.');

    assert(!empty($form[$this->fieldName]), 'Phone number settings is incorrect.');

    if(empty($form[$this->fieldName])) {
      return $form;
    }

    $form['uri'] = [
      '#type' => 'value',
      '#default_value' => $form_state->getValue('uri') ?: $this->opignoSmsOtpProvider->getProvisioningUri(),
    ];

    $form['field_notification_type'] = [
      '#type' => 'value',
      '#default_value' => [['value' => 'sms']],
    ];
    $form['actions']['verify'] = $form['actions']['submit'];
    $form['actions']['verify']['#validate'] = [
      '::validateForm',
    ];
    $form['actions']['verify']['#submit'] = ['::firstStepSubmit'];
    $form['actions']['submit']['#value'] = 'Verify';
    $form['actions']['submit']['#validate'] = [
      '::validateForm',
    ];

    // Apply code element to second step.
    $form['code'] = [
      '#title' => $this->t('One Time Password'),
      '#description' => $this->t('We just sent you a SMS containing a one time login. This code will expire soon.'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 60,
      '#maxlength' => 6,
      '#default_value' => '',
      '#element_validate' => ['::validateCodeElement'],
    ];

    return $form;
  }

}
