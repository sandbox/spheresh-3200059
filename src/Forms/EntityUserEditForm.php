<?php

namespace Drupal\opigno_sms\Forms;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\opigno_sms\MultiStepFormTrait;
use Drupal\opigno_sms\OpignoSmsProvider;
use Drupal\user\AccountForm;
use Drupal\user\UserData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * EntityUserEditForm class.
 */
class EntityUserEditForm extends AccountForm {

  use MultiStepFormTrait;

  /**
   * Constructor object.
   */
  public function __construct(
    OpignoSmsProvider $opigno_sms_otp_provider,
    EntityRepositoryInterface $entity_repository,
    LanguageManagerInterface $language_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    TimeInterface $time = NULL,
    ConfigFactory $config_factory,
    UserData $user_data
  ) {
    parent::__construct($entity_repository, $language_manager, $entity_type_bundle_info, $time);

    $this->opignoSmsOtpProvider = $opigno_sms_otp_provider;
    $this->opignoSmsSmsconfig = $config_factory->get('opigno_sms.sms_config');
    $this->userData = $user_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('opigno_sms.otp_provider'),
      $container->get('entity.repository'),
      $container->get('language_manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('config.factory'),
      $container->get('user.data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    if (!$this->getSmsFieldSettings()) {
      return $form;
    }

    assert(!empty($this->fieldName), 'Phone number settings is incorrect.');

    assert(!empty($form[$this->fieldName]), 'Phone number settings is incorrect.');

    if(empty($form[$this->fieldName])) {
      return $form;
    }

    $form['uri'] = [
      '#type' => 'value',
      '#default_value' => $form_state->getValue('uri') ?: $this->opignoSmsOtpProvider->getProvisioningUri(),
    ];

    $form['actions']['verify'] = $form['actions']['submit'];
    $form['actions']['verify']['#validate'] = [
      '::validateForm',
    ];
    $form['actions']['verify']['#submit'] = ['::firstStepSubmit'];
    $form['actions']['submit']['#value'] = 'Verify';
    $form['actions']['submit']['#validate'] = [
      '::validateForm',
    ];

    // Apply code element to second step.
    $form['code'] = [
      '#title' => $this->t('One Time Password'),
      '#description' => $this->t('We just sent you a SMS containing a one time login. This code will expire soon.'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 60,
      '#maxlength' => 6,
      '#default_value' => '',
      '#element_validate' => ['::validateCodeElement'],
    ];

    return $form;
  }

}
