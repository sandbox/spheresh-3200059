<?php

namespace Drupal\opigno_sms;

use Drupal\Core\Entity\EntityInterface;

/**
 * EntityExtender class.
 */
class UserEntityExtender {

  /**
   * The user entity interface.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $originEntity = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getInstance(EntityInterface $entity) {
    $this->setEntity($entity);
    return $this;
  }

  /**
   * Getter of user entity.
   */
  public function getEntity() {
    if (!$this->originEntity) {
      throw new \Exception('Original entity is empty.');
    }
    return $this->originEntity;
  }

  /**
   * Setter of user entity.
   */
  protected function setEntity(EntityInterface $entity) {
    $this->originEntity = $entity;
  }

  /**
   * Check for the users< if posible to send sms.
   */
  public function smsSendingIsAllowedAndPrefered($check_is_valid = TRUE): bool {
    return $this->entityHasValidNumber($check_is_valid) && $this->entityHasPreferedOpitionSms();
  }

  /**
   * The phone number setting is valid and filled.
   */
  protected function entityHasValidNumber($check_is_valid) {
    /* @var \Drupal\sms\Provider\PhoneNumberVerification $sms_phone_number_verification */
    // @codingStandardsIgnoreLine
    $sms_phone_number_verification = \Drupal::service('sms.phone_number.verification');
    /* @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $this->getEntity();

    /* @var \Drupal\sms\Entity\PhoneNumberSettings $config */
    $config = $sms_phone_number_verification->getPhoneNumberSettingsForEntity($this->getEntity());

    $field_name = $config->getFieldName('phone_number');
    if (!$entity->hasField($field_name)) {
      return FALSE;
    }

    $phone_number = $entity->{$field_name}->value;
    if (empty($phone_number)) {
      return FALSE;
    }

    if (!OpignoSmsProvider::isValidMobileNumber((string)$phone_number)) {
      return FALSE;
    }

    /* @var \Drupal\sms\Entity\PhoneNumberVerification $phone_verification */
    $phone_verification = $sms_phone_number_verification->getPhoneVerificationByEntity($this->getEntity(), $phone_number);
    if ($check_is_valid) {
      return $phone_verification->getStatus() === TRUE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Checks that the user defines a sms as a default notification provider.
   */
  protected function entityHasPreferedOpitionSms(): bool {
    /* @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $this->getEntity();

    $field_name = 'field_notification_type';
    if (!$entity->hasField($field_name)) {
      return FALSE;
    }

    $notification_type = $entity->{$field_name}->value;
    if (empty($notification_type)) {
      return FALSE;
    }

    return 'sms' == $notification_type;
  }

  /**
   * Getter for email field.
   */
  public function getEmail() {
    return $this->getEntity()->getEmail();
  }

  /**
   * Getter for the number field.
   */
  public function getPhoneNumber() {
    // @TODO The phone_number constant should be replaced by th config value.
    return $this->getEntity()->phone_number->value;
  }

  /**
   * Getter for the recipient value.
   */
  public function getRecipient() {
    return $this->getPhoneNumber();
  }

}
