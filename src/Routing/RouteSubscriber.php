<?php

namespace Drupal\opigno_sms\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    /* @var \Symfony\Component\Routing\Route $route */
    if ($route = $collection->get('user.register')) {
      $route->setDefault('_entity_form', 'user.register_sms');
    }
  }

}
