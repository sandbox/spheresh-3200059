<?php

namespace Drupal\opigno_sms;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * MultiStepFormTrait class.
 */
trait MultiStepFormTrait {

  /**
   * User data storage service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * ImmutableConfig config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $opignoSmsSmsconfig;

  /**
   * {@inheritdoc}
   */
  public function processForm($element, FormStateInterface $form_state, $form) {

    if(empty($form[$this->fieldName])) {
      return parent::processForm($element, $form_state, $form);
    }

    $first_values = &$form_state->getStorage()['first_values'];
    if ($first_values) {
      $restore_values = $form_state->getValues() + (array) $first_values;
      $form_state->setValues($restore_values);
    }

    /* @var \Drupal\user\UserData $user_data */
    $user_data = \Drupal::service('user.data');
    $this->userData = $user_data;
    $user = $this->getEntity($form_state);
    $mail_generated = $this->userData->get('opigno_sms', $user->id(), 'mail_generated');

    $element['#submit_default'] = $element['actions']['submit']['#submit'];
    if ($this->isFirstStep($form_state)) {
      if ($mail_generated) {
        $element['account']['mail']['#default_value'] = '';
      }
      $element['account']['mail']['#required'] = FALSE;
      $element['account']['mail']['#description'] = '';
      assert(!empty($this->fieldName), 'Phone number settings is incorrect.');
      if (isset($form[$this->fieldName]['widget'])) {
        foreach (Element::children($form[$this->fieldName]['widget']) as $child) {
          $element[$this->fieldName]['widget'][$child]['value']['#description'] = $this->t("You'll receive a one time confirmation password to this number.");
        }
      }
      unset($element['code'], $element['actions']['submit']);
    }
    else {
      assert(!empty($this->fieldName), 'Phone number settings is incorrect.');
      if (isset($form[$this->fieldName]['widget'])) {
        foreach (Element::children($form[$this->fieldName]['widget']) as $child) {
          $element[$this->fieldName]['widget'][$child]['#access'] = FALSE;
        }
      }
      $this->hideComponents($element, $form_state);
      unset($element['account'], $element['actions']['verify']);
    }
    return parent::processForm($element, $form_state, $form);

  }

  /**
   * Trying to set the property of phone number settings.
   */
  protected function getSmsFieldSettings(): bool {
    $phone_number_settings = $this->getPhoneNumberSettings();

    if (!$phone_number_settings) {
      return FALSE;
    }

    $this->phoneNumberSettings = $phone_number_settings;
    $this->fieldName = $phone_number_settings->getFieldName('phone_number');

    return TRUE;
  }

  /**
   * Phone number settings getter.
   *
   * @return \Drupal\sms\Entity\PhoneNumberSettingsInterface|false
   *   Phone number setting.
   */
  protected function getPhoneNumberSettings() {
    $entity = $this->getEntity();
    return $this->opignoSmsOtpProvider->getPhoneNumberSettings($entity->getEntityTypeId(), $entity->bundle());
  }

  /**
   * Code validator method.
   *
   * @param array $element
   *   Form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   */
  public function validateCodeElement(array &$element, FormStateInterface $form_state): void {
    $values = $this->getValues($form_state);
    if (empty($values['code'])) {
      $form_state->setError($element, 'Code value cannot be an empty.');
    }
    if (empty($values['uri'])) {
      $form_state->setError($element, 'Code value cannot be verified.');
    }
    if ($values['uri'] && $values['code']) {
      if (!$this->opignoSmsOtpProvider->verifyCodeValue($values['uri'], $values['code'])) {
        $form_state->setError($element, 'Entered code is incorrect or expired.');
      }
    }
    else {
      $form_state->setError($element, 'Entered code is incorrect or expired.');
    }
  }

  /**
   * Calls the default submit handlers.
   *
   * @param array $form
   *   Form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   */
  public function preSaveSubmit(array &$form, FormStateInterface $form_state): void {
    foreach ($form['#submit_default'] as $callback) {
      call_user_func_array($form_state->prepareCallback($callback), [
        &$form,
        &$form_state,
      ]);
    }
  }

  /**
   * Global validator.
   *
   * @param array $form
   *   Form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   Content entity interface.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $hide_mail_error = FALSE;
    $mail_generated = &$form_state->getStorage()['mail_generated'];

    $values = $this->getValues($form_state);
    if (empty($values['mail'])) {
      $form_state->setValue('mail', strtr(
        $this->opignoSmsSmsconfig->get('default_mail_pattern'),
        [
          '[user_name]' => $values['name'],
        ]
      ));
      $hide_mail_error = TRUE;

      $mail_generated = TRUE;
    }
    $result = parent::validateForm($form, $form_state);
    $errors = $form_state->getErrors();

    if (isset($errors['mail']) && $hide_mail_error) {
      $form_state->clearErrors();
      // Remove dafault email error if your entered phone number.
      foreach (array_diff_key($errors, array_flip(['mail'])) as $name => $item) {
        $form_state->setErrorByName($name, $item);
      }
    }
    return $result;
  }

  /**
   * Send sms on first step or skip and submit.
   *
   * @param array $form
   *   Form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   */
  public function firstStepSubmit(array &$form, FormStateInterface $form_state): void {
    $phone_should_be_verified = TRUE;
    $entity = $form_state->getFormObject()->getEntity();
    $values = $this->getValues($form_state);
    if (!$entity->isNew()) {
      $old_phone = $entity->{$this->fieldName}->value;
      if ($values['phone_number'] === $old_phone) {
        $phone_should_be_verified = FALSE;
      }
    }

    if (!OpignoSmsProvider::isValidMobileNumber((string)$values['phone_number'])) {
      $phone_should_be_verified = FALSE;
    }

    if (!$phone_should_be_verified) {
      $this->preSaveSubmit($form, $form_state);
    }
    else {
      $form_state->setRebuild();
      $value = $this->getValues($form_state);
      if ($this->isFirstStep($form_state)) {
        $this->opignoSmsOtpProvider->maybeSendNewCode($value['phone_number'], $value['uri']);
        $step = &$form_state->getStorage()['step'];
        $step++;

        $first_values = &$form_state->getStorage()['first_values'];
        $first_values = array_diff_key($form_state->getValues(), array_flip([
          'form_build_id',
          'form_id',
          'op',
          'submit',
          'verify',
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  private function isFirstStep(FormStateInterface $form_state) {
    return !isset($form_state->getStorage()['step']);
  }

  /**
   * Hides all fieldable components of the form display.
   *
   * @param array $form
   *   Form build array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   */
  protected function hideComponents(array &$form, FormStateInterface $form_state): void {
    $child_name = array_keys($form_state->get('form_display')->getComponents());
    foreach (array_diff($child_name, ['code']) as $item) {
      if (isset($form[$item]['widget']) && Element::children($form[$item]['widget'])) {
        foreach (Element::children($form[$item]['widget']) as $child) {
          $form[$item]['widget'][$child]['#access'] = FALSE;
        }
      }
      else {
        $form[$item]['#access'] = FALSE;
      }
    }
  }

  /**
   * Setter method to receive form state by field mapper.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   *
   * @return array
   *   Mixed form state value.
   */
  protected function getValues(FormStateInterface $form_state): array {
    $values = [];
    $form_values = $form_state->getValues();
    $field_map = $this->getFieldMap();
    foreach ($field_map as $name => $item) {
      $values[$name] = NestedArray::getValue($form_values, explode('.', $item));
    }
    return $values;
  }

  /**
   * Setter method to update form state by field mapper.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface.
   * @param mixed $values
   *   Mixed form state value.
   */
  protected function setValues(FormStateInterface $form_state, $values): void {
    $form_values = $form_state->getValues();
    $field_map = $this->getFieldMap();
    foreach ($field_map as $name => $item) {
      if (isset($values[$name])) {
        NestedArray::setValue($form_values, explode('.', $item), $values[$name]);
      }
    }
    $form_state->setValues($form_values);
  }

  /**
   * Mapper of field value to form state keys.
   */
  protected function getFieldMap(): array {
    return [
      'mail' => 'mail',
      'name' => 'name',
      'uri' => 'uri',
      'code' => 'code',
      'phone_number' => $this->fieldName . '.0.value',
      'notification_type' => 'field_notification_type.0.value',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $mail_generated = $form_state->getStorage()['mail_generated'] ?: FALSE;
    $user = $this->getEntity($form_state);
    $this->userData->set('opigno_sms', $user->id(), 'mail_generated', (int) $mail_generated);
  }

}
