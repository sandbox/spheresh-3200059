<?php

namespace Drupal\opigno_sms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SmsConfigForm.
 */
class SmsConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'opigno_sms.sms_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sms_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('opigno_sms.sms_config');

    $form = parent::buildForm($form, $form_state);
    $form['default_mail_pattern'] = [
      '#type' => 'textfield',
      '#title' => 'Default mail pattern',
      '#default_value' => $form_state->getValue('default_mail_pattern') ?: $config->get('default_mail_pattern'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('opigno_sms.sms_config')
      ->set('default_mail_pattern', $form_state->getValue('default_mail_pattern'))
      ->save();
  }

}
