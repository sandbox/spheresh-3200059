<?php

namespace Drupal\Tests\opigno_sms\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\user\Entity\User;
use OTPHP\TOTP;

/**
 * {@inheritdoc}
 *
 * @runTestsInSeparateProcesses
 *
 * @group opigno_sms
 */
class OpignoSmsMailTest extends KernelTestBase {

  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'user',
    'sms',
    'system',
    'field',
    'options',
    'dynamic_entity_reference',
    'telephone',
    'sms_twilio',
    'opigno_sms',
    'opigno_sms_messages',
    'opigno_sms_token',
    'opigno_sms_test',
  ];

  protected $otp;

  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('sms');
    $this->installEntitySchema('sms_phone_number_verification');

    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);

    $this->installConfig(['opigno_sms_test']);
    $this->installConfig(['opigno_sms']);

    $this->formBuilder = $this->container->get('form_builder');

    $this->otp = (new TOTP('uri', NULL, 3600));
  }

  /**
   * {@inheritdoc}
   */
  public function testCreate1() {
    $user = User::create([
      'name' => 'admin',
      'mail' => 'admin@example.com',
      'phone_number' => ['value' => '+123456789000'],
      'field_notification_type' => ['value' => 'sms'],
    ]);
    $user->save();
    $user2 = User::create([
      'name' => 'admin2',
      'mail' => 'admin2@example.com',
      'phone_number' => ['value' => '+123456789000'],
      'field_notification_type' => ['value' => 'mail'],
    ]);
    $user2->save();
    /* @var MailManager $mail */
    $mail = \Drupal::service('plugin.manager.mail');
    $res = $mail->mail('user', 'register_no_approval_required', 'admin@example.com', '', ['account' => $user]);
    $this->assertTrue($res['result']);
    \Drupal::currentUser()->setAccount($user);
    $res = $mail->mail('user', 'register_no_approval_required', 'admin2@example.com', '', ['account' => $user2]);
    $this->assertTrue($res['result']);

    // @TODO Check create user here.
  }

}
