<?php

namespace Drupal\Tests\opigno_sms\Kernel;

use Drupal\Core\Form\FormState;
use Drupal\KernelTests\KernelTestBase;
use OTPHP\TOTP;

/**
 * {@inheritdoc}
 *
 * @runTestsInSeparateProcesses
 * @covers \Drupal\opigno_sms\Forms\RegisterForm
 * @covers \Drupal\opigno_sms\Forms\EntityUserEditForm
 * @covers \Drupal\opigno_sms\OpignoSmsProvider
 *
 * @group opigno_sms
 */
class OpignoSmsEditFormTest extends KernelTestBase {

  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'user',
    'sms',
    'system',
    'field',
    'options',
    'dynamic_entity_reference',
    'telephone',
    'opigno_sms',
    'sms_twilio',
    'opigno_sms_messages',
    'opigno_sms_token',
    'opigno_sms_test',
  ];

  protected $otp;

  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('sms');
    $this->installEntitySchema('sms_phone_number_verification');

    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);

    $this->installConfig(['opigno_sms_test']);
    $this->installConfig(['opigno_sms']);

    $this->formBuilder = $this->container->get('form_builder');

    $this->otp = (new TOTP('uri', NULL, 3600));
  }

  /**
   * {@inheritdoc}
   */
  public function testCreate() {

    try {
      $input = $this->inputFirstWithValidPhone();
      $this->entityTypeManager = $this->container->get('entity_type.manager');
      $entity = $this->entityTypeManager->getStorage('user')->create($input);
      $entity->save();

      // First step validation submit.
      $first_state = new FormState();
      $first_state->setProgrammed();

      $input['current_pass'] = '123';
      $first_state->setUserInput($input);
      $first_state->setValues($input);

      \Drupal::currentUser()->setAccount($entity);

      $form_object = $this->entityTypeManager->getFormObject('user', 'default');
      $form_object->setEntity($entity);
      // @TODO Add some form validation.
      $this->formBuilder->buildForm($form_object, $first_state);
      $this->assertTrue(isset($first_state->getErrors()['pass']));
    }
    catch (\Exception $e) {

    }

    // @TODO Check create user here.
  }

  /**
   * {@inheritdoc}
   */
  private function inputFirstWithValidPhone() {
    return [
      'phone_number' =>
        [
          [
            'value' => '+123456789000',
          ],
        ],
      'form_id' => 'user_register_sms_form',
      'mail' => 'admin@example.com',
      'name' => 'user_name',
      'pass' =>
        [
          'pass1' => '123',
          'pass2' => '123',
        ],
      'op' => 'Create new account',
      'uri' => $this->otp->getProvisioningUri(),
      'field_notification_type' => NULL,
    ];
  }

}
