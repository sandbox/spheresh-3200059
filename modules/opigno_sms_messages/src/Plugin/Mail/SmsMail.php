<?php

namespace Drupal\opigno_sms_messages\Plugin\Mail;

use Drupal\Core\Mail\MailInterface;

/**
 * Provides a 'Mailer' plugin to send sms.
 *
 * @Mail(
 *   id = "smsmailer",
 *   label = @Translation("SMS Mailer"),
 *   description = @Translation("SMS Mailer Plugin.")
 * )
 */
class SmsMail implements MailInterface {

  /**
   * {@inheritdoc}
   */
  public function format(array $message) {
    // TODO: Implement format() method.
  }

  /**
   * {@inheritdoc}
   */
  public function mail(array $message) {
    // TODO: Implement mail() method.
  }

}
