<?php

namespace Drupal\opigno_sms_messages;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the storage handler class for Sms Message entities.
 *
 * This extends the base storage class, adding required special handling for
 * Sms Message entities.
 *
 * @ingroup opigno_sms_messages
 */
interface SmsMessageEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getByModuleKey($module, $key);

}
