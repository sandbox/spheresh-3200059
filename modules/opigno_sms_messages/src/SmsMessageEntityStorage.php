<?php

namespace Drupal\opigno_sms_messages;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for Sms Message entities.
 *
 * This extends the base storage class, adding required special handling for
 * Sms Message entities.
 *
 * @ingroup opigno_sms_messages
 */
class SmsMessageEntityStorage extends SqlContentEntityStorage implements SmsMessageEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getByModuleKey($module, $key) {
    $entity = FALSE;
    try {
      $entity = $this->loadByProperties([
        'field_module_key.value' => $module . ':' . $key,
      ]);
    }
    catch (\Exception $e) {
    }
    return $entity ? reset($entity) : FALSE;
  }

}
