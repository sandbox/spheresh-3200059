<?php

namespace Drupal\opigno_sms_messages;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for sms_message_entity.
 */
class SmsMessageEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
