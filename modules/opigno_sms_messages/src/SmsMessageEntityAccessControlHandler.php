<?php

namespace Drupal\opigno_sms_messages;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Sms Message Entity entity.
 *
 * @see \Drupal\opigno_sms_messages\Entity\SmsMessageEntity.
 */
class SmsMessageEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\opigno_sms_messages\Entity\SmsMessageEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished sms message entity entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published sms message entity entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit sms message entity entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete sms message entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add sms message entity entities');
  }

}
