<?php

namespace Drupal\opigno_sms_messages\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * MessengerEvent class.
 */
class MessengerEvent extends Event {

  const ADD_MESSAGE = 'messenger_event.add_message';

  public $message;

  public $type;

  public $repeat;

  /**
   * MessengerEvent constructor.
   */
  public function __construct($message, string $type, bool $repeat) {
    $this->message = $message;
    $this->type = $type;
    $this->repeat = $repeat;

  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getRepeat() {
    return $this->repeat;
  }

}
