<?php

namespace Drupal\opigno_sms_messages\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * SmsMailEv class.
 */
class SmsMailEvent extends Event {

  const SEND_SMS = 'sms_mail_event.send_sms';

  protected $module;

  protected $key;

  protected $langcode;

  protected $params;

  protected $reply;

  protected $send;

  protected $isMailNotificationAllowed = TRUE;

  protected $entity = FALSE;

  /**
   * SmsMailEvent constructor.
   */
  public function __construct(string $module, string $key, string $to, string $langcode, array $params, $reply, bool $send) {
    $this->module = $module;
    $this->key = $key;
    $this->to = $to;
    $this->langcode = $langcode;
    $this->params = $params;
    $this->reply = $reply;
    $this->send = $send;
  }

  /**
   * {@inheritdoc}
   */
  public function getResult() {
    return [
      'result' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isMailNotificationAllowed(): bool {
    return $this->isMailNotificationAllowed;
  }

  /**
   * {@inheritdoc}
   */
  public function setIsMailNotificationAllowed(bool $isMailNotificationAllowed): void {
    $this->isMailNotificationAllowed = $isMailNotificationAllowed;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isImidiateNotificationRequired() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity($entity) {
    $this->entity = $entity;
  }

}
