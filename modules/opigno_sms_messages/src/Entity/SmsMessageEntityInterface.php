<?php

namespace Drupal\opigno_sms_messages\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Sms Message Entity entities.
 *
 * @ingroup opigno_sms_messages
 */
interface SmsMessageEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Sms Message Entity name.
   *
   * @return string
   *   Name of the Sms Message Entity.
   */
  public function getName();

  /**
   * Sets the Sms Message Entity name.
   *
   * @param string $name
   *   The Sms Message Entity name.
   *
   * @return \Drupal\opigno_sms_messages\Entity\SmsMessageEntityInterface
   *   The called Sms Message Entity entity.
   */
  public function setName($name);

  /**
   * Gets the Sms Message Entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Sms Message Entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Sms Message Entity creation timestamp.
   *
   * @param int $timestamp
   *   The Sms Message Entity creation timestamp.
   *
   * @return \Drupal\opigno_sms_messages\Entity\SmsMessageEntityInterface
   *   The called Sms Message Entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Getter of body value.
   */
  public function getBody();

}
