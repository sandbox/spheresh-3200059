<?php

namespace Drupal\opigno_sms_messages\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Sms Message Entity entity.
 *
 * @ingroup opigno_sms_messages
 *
 * @ContentEntityType(
 *   id = "sms_message_entity",
 *   label = @Translation("Sms Message"),
 *   handlers = {
 *     "storage" = "Drupal\opigno_sms_messages\SmsMessageEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\opigno_sms_messages\SmsMessageEntityListBuilder",
 *     "views_data" = "Drupal\opigno_sms_messages\Entity\SmsMessageEntityViewsData",
 *     "translation" = "Drupal\opigno_sms_messages\SmsMessageEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\opigno_sms_messages\Form\SmsMessageEntityForm",
 *       "add" = "Drupal\opigno_sms_messages\Form\SmsMessageEntityForm",
 *       "edit" = "Drupal\opigno_sms_messages\Form\SmsMessageEntityForm",
 *       "delete" = "Drupal\opigno_sms_messages\Form\SmsMessageEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\opigno_sms_messages\SmsMessageEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\opigno_sms_messages\SmsMessageEntityAccessControlHandler",
 *   },
 *   base_table = "sms_message_entity",
 *   data_table = "sms_message_entity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer sms message entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "field_module_key",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/sms_message_entity/{sms_message_entity}",
 *     "add-form" = "/admin/structure/sms_message_entity/add",
 *     "edit-form" = "/admin/structure/sms_message_entity/{sms_message_entity}/edit",
 *     "delete-form" = "/admin/structure/sms_message_entity/{sms_message_entity}/delete",
 *     "collection" = "/admin/structure/sms_message_entity",
 *   },
 *   field_ui_base_route = "sms_message_entity.settings"
 * )
 */
class SmsMessageEntity extends ContentEntityBase implements SmsMessageEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['status']->setDescription(t('A boolean indicating whether the Sms Message Entity is published.'))
      ->setDisplayConfigurable('form', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDefaultValue(1)
      ->setInitialValue(1)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getBody() {
    return $this->field_body->value;
  }

}
