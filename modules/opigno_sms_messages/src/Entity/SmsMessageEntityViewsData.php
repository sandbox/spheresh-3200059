<?php

namespace Drupal\opigno_sms_messages\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Sms Message Entity entities.
 */
class SmsMessageEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
