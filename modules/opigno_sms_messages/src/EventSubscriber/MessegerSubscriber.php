<?php

namespace Drupal\opigno_sms_messages\EventSubscriber;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\opigno_sms_messages\Event\MessengerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Class MessegerSubscriber.
 */
class MessegerSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new MessegerSubscriber object.
   */
  public function __construct(AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MessengerEvent::ADD_MESSAGE] = ['messengerEventAddMessage'];
    return $events;
  }

  /**
   * This method is called when the messenger_event.add_message is dispatched.
   *
   * @param \Drupal\opigno_sms_messages\Event\MessengerEvent $event
   *   The dispatched event.
   */
  public function messengerEventAddMessage(MessengerEvent $event): void {
    if ($event->message instanceof TranslatableMarkup) {
      $string = str_replace([
        'your email address',
      ], [
        'you',
      ], $event->message->getUntranslatedString());
      // @codingStandardsIgnoreLine
      $event->message = new TranslatableMarkup($string, $event->message->getArguments(), $event->message->getOptions());
    }
  }

}
