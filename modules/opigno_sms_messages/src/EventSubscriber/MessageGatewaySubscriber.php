<?php

namespace Drupal\opigno_sms_messages\EventSubscriber;

use Drupal\sms\Entity\SmsGateway;
use Drupal\sms\Event\RecipientGatewayEvent;
use Drupal\sms\Event\SmsEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class MessageGatewaySubscriber.
 */
class MessageGatewaySubscriber implements EventSubscriberInterface {

  /**
   * Constructs a new MessageGatewaySubscriber object.
   */
  public function addDefaultGateway(RecipientGatewayEvent $event) {
    $gateway = SmsGateway::load('twilio');
    if ($gateway) {
      $event->addGateway($gateway, 200);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SmsEvents::MESSAGE_GATEWAY => [['addDefaultGateway']],
    ];
  }

}
