<?php

namespace Drupal\opigno_sms_messages;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Sms Message Entity entities.
 *
 * @ingroup opigno_sms_messages
 */
class SmsMessageEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Sms Message Entity ID');
    $header['name'] = $this->t('Name');
    $header['body'] = $this->t('Body');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\opigno_sms_messages\Entity\SmsMessageEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.sms_message_entity.edit_form',
      ['sms_message_entity' => $entity->id()]
    );
    $row['body'] = $entity->getBody();
    return $row + parent::buildRow($entity);
  }

}
