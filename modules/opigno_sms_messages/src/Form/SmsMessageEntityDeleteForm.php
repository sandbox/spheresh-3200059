<?php

namespace Drupal\opigno_sms_messages\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Sms Message Entity entities.
 *
 * @ingroup opigno_sms_messages
 */
class SmsMessageEntityDeleteForm extends ContentEntityDeleteForm {


}
