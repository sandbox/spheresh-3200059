<?php

namespace Drupal\opigno_sms_messages\Decorators;

use Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface;
use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\opigno_sms_messages\OpignoSmsMessagesProvider;
use Drupal\opigno_sms_messages\Event\SmsMailEvent;
use Drupal\sms\Direction;
use Drupal\sms\Entity\SmsMessage;
use Drupal\sms\Provider\SmsProviderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * {@inheritdoc}
 */
class MailManager extends PluginManagerBase implements PluginManagerInterface, CachedDiscoveryInterface, CacheableDependencyInterface, MailManagerInterface {

  /**
   * Service "plugin.manager.mail" interface.
   *
   * @var \Drupal\Core\Mail\MailManager
   */
  protected $inner;

  /**
   * Service "event_dispatcher" interface.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcher
   */
  protected $eventDispatcher;

  protected $smsProvider;

  protected $opignoSmsMessagesProvider;

  /**
   * {@inheritdoc}
   */
  public function __construct($inner, EventDispatcherInterface $event_dispatcher, SmsProviderInterface $sms_provider, OpignoSmsMessagesProvider $opigno_sms_messages_provider) {
    $this->inner = $inner;
    $this->eventDispatcher = $event_dispatcher;
    $this->smsProvider = $sms_provider;
    $this->opignoSmsMessagesProvider = $opigno_sms_messages_provider;
  }

  /**
   * {@inheritdoc}
   */
  public function mail($module, $key, $to, $langcode, $params = [], $reply = NULL, $send = TRUE) {
    $event = new SmsMailEvent($module, $key, $to, $langcode, $params, $reply, $send);
    $entity = $this->opignoSmsMessagesProvider->loadEntityByPhoneNumber($to);
    if ($entity) {
      // @codingStandardsIgnoreLine
      $entity_extender = \Drupal::service('opigno_sms.entity_extender')
        ->getInstance($entity);
      /* @var \Drupal\opigno_sms\UserEntityExtender $entity_extender */
      if (isset($entity_extender) && $entity_extender->smsSendingIsAllowedAndPrefered()) {

        $message = $this->doSms($module, $key, $to, $langcode, $params, $reply, $send);

        /* @var \Drupal\sms\Entity\SmsMessage $sms_message */
        $sms_message = SmsMessage::create()
          ->setDirection(Direction::OUTGOING)
          ->setMessage($this->opignoSmsMessagesProvider->getMessage($message))
          ->addRecipient($this->getPhoneNumber($entity));

        if (!$event->isImidiateNotificationRequired()) {
          $this->smsProvider->queue($sms_message);
        }
        else {
          $this->smsProvider->send($sms_message);
        }
        return $event->getResult();

      }

    }
    return $this->inner->mail($module, $key, $to, $langcode, $params, $reply, $send);

  }

  /**
   * Getter for the number field.
   */
  public function getPhoneNumber($entity) {
    // @TODO The phone_number constant should be replaced by th config value.
    return $entity->phone_number->value;
  }

  /**
   * {@inheritdoc}
   */
  public function doSms($module, $key, $to, $langcode, $params = [], $reply = NULL, $send = TRUE) {

    // Bundle up the variables into a structured array for altering.
    $message = [
      'id' => $module . '_' . $key,
      'module' => $module,
      'key' => $key,
      'to' => $to,
      'langcode' => $langcode,
      'params' => $params,
      'send' => TRUE,
      'subject' => '',
      'body' => [],
    ];

    // Build the email (get subject and body, allow additional headers) by
    // invoking hook_mail() on this module. We cannot use
    // moduleHandler()->invoke() as we need to have $message by reference in
    // hook_mail().
    if (function_exists($function = $module . '_mail')) {
      $function($key, $message, $params);
    }

    // Invoke hook_mail_alter() to allow all modules to alter the resulting
    // email.
    // @codingStandardsIgnoreLine
    \Drupal::service('module_handler')->alter('mail', $message);
    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return $this->inner->getCacheContexts();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return $this->inner->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return $this->inner->getCacheMaxAge();
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions() {
    return $this->inner->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function useCaches($use_caches = FALSE) {
    return $this->inner->useCaches();
  }

  /**
   * {@inheritdoc}
   */
  public function getDiscovery() {
    return $this->inner->getDiscovery();
  }

}
