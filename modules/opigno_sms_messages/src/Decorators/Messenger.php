<?php

namespace Drupal\opigno_sms_messages\Decorators;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\opigno_sms_messages\Event\MessengerEvent;

/**
 * Messenger class.
 */
class Messenger implements MessengerInterface {

  protected $inner;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $inner) {
    $this->inner = $inner;
  }

  /**
   * {@inheritdoc}
   */
  public function addMessage($message, $type = self::TYPE_STATUS, $repeat = FALSE) {

    // Instantiate our event.
    $event = new MessengerEvent($message, $type, $repeat);

    // Get the event_dispatcher service and dispatch the event.
    // @codingStandardsIgnoreLine
    $event_dispatcher = \Drupal::service('event_dispatcher');
    $event_dispatcher->dispatch(MessengerEvent::ADD_MESSAGE, $event);
    return $this->inner->addMessage($event->getMessage(), $event->getType(), $event->getRepeat());
  }

  /**
   * {@inheritdoc}
   */
  public function addStatus($message, $repeat = FALSE) {
    return $this->inner->addStatus($message, $repeat);
  }

  /**
   * {@inheritdoc}
   */
  public function addError($message, $repeat = FALSE) {
    return $this->inner->addError($message, $repeat);
  }

  /**
   * {@inheritdoc}
   */
  public function addWarning($message, $repeat = FALSE) {
    return $this->inner->addWarning($message, $repeat);
  }

  /**
   * {@inheritdoc}
   */
  public function all() {
    return $this->inner->all();
  }

  /**
   * {@inheritdoc}
   */
  public function messagesByType($type) {
    return $this->inner->messagesByType($type);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll() {
    return $this->inner->deleteAll();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteByType($type) {
    return $this->inner->deleteByType($type);
  }

}
