<?php

namespace Drupal\opigno_sms_messages;

use Drupal\Core\Entity\EntityTypeManager;

/**
 * OpignoSmsMessagesProvider class.
 */
class OpignoSmsMessagesProvider {

  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entity_typemanager) {
    $this->entityTypeManager = $entity_typemanager;
  }

  /**
   * {@inheritdoc}
   */
  public function loadEntityByPhoneNumber($to) {
    if (!($user_storage = $this->getUserStorage())) {
      return FALSE;
    }
    $users = $user_storage->loadByProperties(['mail' => $to]);
    if (!$users) {
      return FALSE;
    }
    $user = reset($users);
    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage(array $message): string {
    if (!($sms_message_storage = $this->getSmsMessageEntity())) {
      return implode('', $message['body']);
    }
    /* @var \Drupal\opigno_sms_messages\Entity\SmsMessageEntityInterface $entity */
    $entity = $sms_message_storage
      ->getByModuleKey($message['module'], $message['key']);
    if ($entity) {
      $body = $entity->getBody();
      // @codingStandardsIgnoreLine
      \Drupal::moduleHandler()->alter('token_sms', $body, $message);
      // @codingStandardsIgnoreLine
      \Drupal::moduleHandler()
        ->alter('token_sms_' . $message['module'], $body, $message);
      return $body;
    }
    return implode('', $message['body']);
  }

  /**
   * {@inheritdoc}
   */
  private function getUserStorage() {
    try {
      return $this->entityTypeManager->getStorage('user');
    }
    catch (\InvalidPluginDefinitionException $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  private function getSmsMessageEntity() {
    try {
      return $this->entityTypeManager->getStorage('sms_message_entity');
    }
    catch (\InvalidPluginDefinitionException $e) {
      return FALSE;
    }
  }

}
