<?php

namespace Drupal\opigno_sms_debug\Plugin\SmsGateway;

use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\Plugin\SmsGatewayPluginInterface;
use Drupal\sms_twilio\Plugin\SmsGateway\Twilio as DefaultTwilio;

/**
 * {@inheritdoc}
 */
class Twilio extends DefaultTwilio implements SmsGatewayPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function send(SmsMessageInterface $sms) {
    // @TODO  It should be removed.
    $result = new SmsMessageResult();
    $report = new SmsDeliveryReport();
    foreach ($sms->getRecipients() as $recipient) {
      \Drupal::messenger()
        ->addMessage(sprintf('SMS FOR %s: %s', $recipient, $sms->getMessage()));
      $report->setRecipient($recipient);
      $result->addReport($report);
    }
    return $result;
  }

}
