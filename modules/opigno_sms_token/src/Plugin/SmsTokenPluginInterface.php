<?php

namespace Drupal\opigno_sms_token\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Sms token plugin plugins.
 */
interface SmsTokenPluginInterface extends PluginInspectionInterface {

  /**
   * Returns human readable option key.
   */
  public function getKey();

  /**
   * Return options list.
   */
  public function getOptions();

  /**
   * Check if it is math a module.
   */
  public function match($module);

  /**
   * Replace token hook.
   */
  public function replace(&$body, &$message);

  /**
   * Return available tokens.
   */
  public function getTokens(): array;

}
