<?php

namespace Drupal\opigno_sms_token\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Sms token plugin plugins.
 */
abstract class SmsTokenPluginBase extends PluginBase implements SmsTokenPluginInterface {

}
