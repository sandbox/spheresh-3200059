<?php

namespace Drupal\opigno_sms_token\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Sms token plugin plugin manager.
 */
class SmsTokenPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new SmsTokenPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/SmsTokenPlugin', $namespaces, $module_handler, 'Drupal\opigno_sms_token\Plugin\SmsTokenPluginInterface', 'Drupal\opigno_sms_token\Annotation\SmsTokenPlugin');

    $this->alterInfo('opigno_sms_token_sms_token_plugin_info');
    $this->setCacheBackend($cache_backend, 'opigno_sms_token_sms_token_plugin_plugins');
  }

}
