<?php

namespace Drupal\opigno_sms_token\Plugin\SmsTokenPlugin;

use Drupal\opigno_sms_token\Plugin\SmsTokenPluginBase;
use Drupal\opigno_sms_token\Plugin\SmsTokenPluginInterface;

/**
 * Opigno token plugin.
 *
 * @SmsTokenPlugin(
 *  id = "opigno_sms_token_plugin",
 *  label = @Translation("Opigno sms token plugin"),
 * )
 */
class OpignoSmsTokenPlugin extends SmsTokenPluginBase implements SmsTokenPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return 'Opigno';
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    return [
      "opigno_learning_path:training_expiring_certification" => 'Learning Path: Training Expiring Certification',
      "opigno_learning_path:opigno_learning_path_add_membership" => 'Learning Path: Add Membership',
      "opigno_learning_path:opigno_learning_path_membership_needs_validate" => 'Learning Path: Membership Needs Validate',
      'opigno_moxtra:upcoming_meeting_notify' => 'Moxtra: Upcoming Meeting Notify',
      'opigno_ilt:upcoming_ilt_notify' => 'ILT: Upcoming ILT Notify',
      'opigno_messaging:message_notification' => 'Messaging: Message Notification',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function match($module) {
    return in_array($module, [
      'opigno_learning_path',
      'opigno_moxtra',
      'opigno_ilt',
      'opigno_messaging',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function replace(&$body, &$message) {

  }

  /**
   * {@inheritdoc}
   */
  public function getTokens(): array {
    return [];
  }

}
