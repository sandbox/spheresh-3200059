<?php

namespace Drupal\opigno_sms_token\Plugin\SmsTokenPlugin;

use Drupal\opigno_sms_token\Plugin\SmsTokenPluginBase;
use Drupal\opigno_sms_token\Plugin\SmsTokenPluginInterface;

/**
 * User token plugin.
 *
 * @SmsTokenPlugin(
 *  id = "users_sms_token_plugin",
 *  label = @Translation("Users sms token plugin"),
 * )
 */
class UsersSmsTokenPlugin extends SmsTokenPluginBase implements SmsTokenPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return 'User';
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    return [
      'user:cancel_confirm' => 'Cancel Confirm',
      'user:password_reset' => 'Password Reset',
      'user:register_admin_created' => 'Register Admin Created',
      'user:register_no_approval_required' => 'Register No Approval Required',
      'user:register_pending_approval' => 'Register Pending Approval',
      'user:register_pending_approval_admin' => 'Register Pending Approval Admin',
      'user:status_activated' => 'Status Activated',
      'user:status_blocked' => 'Status Blocked',
      'user:status_canceled' => 'Status Canceled',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function match($module) {
    return in_array($module, ['user']);
  }

  /**
   * {@inheritdoc}
   */
  public function replace(&$body, &$message) {
    $langcode = $message['langcode'];
    $variables = ['user' => $message["params"]['account']];
    $token_options = [
      'langcode' => $langcode,
      'callback' => 'user_mail_tokens',
      'clear' => TRUE,
    ];
    $body = \Drupal::token()->replace($body, $variables, $token_options);
  }

  /**
   * {@inheritdoc}
   */
  public function getTokens(): array {
    return ['user'];
  }

}
