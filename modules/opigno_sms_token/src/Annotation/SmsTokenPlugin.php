<?php

namespace Drupal\opigno_sms_token\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Sms token plugin item annotation object.
 *
 * @see \Drupal\opigno_sms_token\Plugin\SmsTokenPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class SmsTokenPlugin extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
